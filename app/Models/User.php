<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
	
	protected $table = 'users';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

    protected $fillable = ['deleted_at','created_at','updated_at','deal_title','deal_name','deal_address','deal_phone','deal_price','deal_price','deal_image','deal_description','deal_order','deal_archived','deal_type','latitude', 'longitude'];

	protected $dates = ['deleted_at'];
}
