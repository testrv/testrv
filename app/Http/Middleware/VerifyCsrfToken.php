<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/signup',
        'auth/register',
        '/auth/fbregister',
        '/auth/login',
        '/auth/forgetpassword',
        'auth/postDeal',
        'auth/userProfile',
        'auth/changePassword',
        'auth/sociallogin',
        'auth/postfblogin',
        'auth/postNotification',
        'auth/reportBug',
        'auth/dealReport',
        'auth/fcmupdate'

    ];
}
