<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Featured_Deal;
use App\Models\Featured_Deal_Api;
use App\Models\Deal_User;
Use Hash;
use App\Http\Controllers\AuthenticateController;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use Mail;

class UserController extends Controller
{

    public $show_action = true;
    public $view_col = 'deal_order';
    public $listing_cols = ['id', 'deal_title', 'deal_name', 'deal_address', 'deal_phone', 'deal_price', 'deal_image', 'deal_description', 'deal_order', 'deal_archived'];

    public function __construct() {
        // Field Access of Listing Columns
        if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
            $this->middleware(function ($request, $next) {
                $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
                return $next($request);
            });
        } else {
            $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
        }
    }

    public function home(){

        if (Auth::check()) {
            $user = Auth::user();
            return $user->id;
        }else{
            return 'Not logged in';
        }

    }

    public function LoginFailDetected(){
        return 'Invalid login credentials supplied';
    }

    public function RegistrationFailDetected(){

        if(isset($errors)){
            return json_decode($errors);
        }
    }

    public function login(AuthenticateController $authenticateController,Request $request){

        $result = $authenticateController->authenticate($request);

        $result =   json_decode($result->getContent());

        $result =   (array) $result;


        //echo "<pre>"; print_r($result); die;

        if(array_key_exists('token',$result)){

            $user = JWTAuth::toUser($result['token']);
            $result['email']=$user->email;
            $result['username']=$user->username;
            $result['id']=$user->id;
            User::where('id','=',$user->id)->update(array('active'=>1));

            $userdata = User::select('users.id','users.name','users.context_id','users.email','users.type','users.username','school_list.school_name','users.school','users.program','users.facebook_id','users.active','users.fcm_token','users.school_email')->join('school_list', 'users.school', '=', 'school_list.id')->where('users.id','=',$user->id)->first();
            //$userdata = User::join('school_list', 'users.school', '=', 'school_list.id')->where('users.id','=',$user->id)->first();
            if($userdata){
                $userdata = $userdata->toArray();
            }

            $userdata['school'] = $userdata['school_name'];
            $final  =   response()->json(['message' => 'success', 'token' => $result['token'],'data'=>$userdata ]);

            return $final;
        }else{
            $final  =   response()->json(['message' => 'failure', 'data' => $result ]);
            return $final;
        }
    }



    public function postSociallogin(Request $request) {
        $post_data=$request->all();
        $count1=User::where('email','=',$request->input('email'))->where('facebook_id','=',$request->input('facebook_id'))->count();
        $count2=User::where('email','!=',$request->input('email'))->where('facebook_id','=',$request->input('facebook_id'))->count();
        $count3=User::where('email','=',$request->input('email'))->where('facebook_id','!=',$request->input('facebook_id'))->count();

        $school_id = $request->input('school');

        if($count1 && !$count2 && !$count3)
        {
            $userobj=User::where('email','=',$request->input('email'))->where('facebook_id','=',$request->input('facebook_id'))->first();
        }
        else if(!$count1 && $count2 && !$count3)
        {
            $userobj=User::where('email','!=',$request->input('email'))->where('facebook_id','=',$request->input('facebook_id'))->first();
            $userobj->email=$post_data['email'];
        }
        else if(!$count1 && !$count2 && $count3)
        {
            $userobj=User::where('email','=',$request->input('email'))->where('facebook_id','!=',$request->input('facebook_id'))->first();
            $userobj->facebook_id=$post_data['facebook_id'];
        }
        else
        {
            $userobj=new User;
            $userobj->email=$post_data['email'];
            $userobj->username=$post_data['name'].$post_data['facebook_id'];
            $userobj->password=bcrypt(rand(999999,6));
            $userobj->active=1;
            $userobj->name = $request->input('name');
            $userobj->password = Hash::make($this->randomPassword());
            $userobj->school = $school_id;
            $userobj->program = $request->input('program');
            $userobj->email = $request->input('email');
            $userobj->facebook_id=$post_data['facebook_id'];
            $userobj->school_email = $request->input('school_email');

        }
        $userobj->save();


        $school = DB::select(DB::raw('SELECT school_name FROM school_list where id ='.$school_id) );

        $userdata = $userobj->getOriginal();

        $userdata['school'] = $school[0]->school_name;

        $token = JWTAuth::fromUser($userobj);
        $result = ['message' => 'success', 'data' => [ 'token' =>  $token,'userinfo'=>$userdata] ];

        return $result;

    }


    /*
    for registration
    */
    public function postRegister(Request $request)
    {

        $rules = Module::validateRules("users", $request);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return json_encode(['message' => 'failure', 'data' => $validator->errors()->all()]);
        }

        $userdata=User::where('username','=',$request->input('username'))->first();

        if(isset($userdata)){
            return json_encode(['message' => 'failure', 'data' => ['Username Already Exists!']]);
        }

        $useremail=User::where('email','=',$request->input('school_email'))->first();

        if(isset($useremail)){
            return json_encode(['message' => 'failure', 'data' => ['User Email Already Exists!']]);
        }
        $school_id = $request->input('school');
        $userdata = new User;
        $userdata->username = $request->input('username');
        $userdata->name = $request->input('name');
        $userdata->password =Hash::make($request->input('password'));
        $userdata->school = $school_id;
        $userdata->program = $request->input('program');
        $userdata->email = $request->input('school_email');
        $userdata->school_email = $request->input('school_email');
        $userdata->save();

        $token = JWTAuth::fromUser($userdata);

        $school = DB::select(DB::raw('SELECT school_name FROM school_list where id ='.$school_id) );

        $userdata = $userdata->getOriginal();

        $userdata['school'] = $school[0]->school_name;

        $data = ['message' => 'success', 'data' => $userdata,'token' =>  $token ];

        return $data;
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function Sociallogin(Request $request)
    {

        $rules = Module::validateRules("users", $request);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return json_encode(['message' => 'failure', 'data' => $validator->errors()->all()]);
        }

        $useremail=User::where('email','=',$request->input('email'))->first();

        if(isset($useremail)){
            return json_encode(['message' => 'failure', 'data' => 'User Email Already Exists!']);
        }

        $userdata = new User;
        $userdata->username = $request->input('name').rand(10,100);
        $userdata->name = $request->input('name');
        $userdata->password = Hash::make($this->randomPassword());
        $userdata->school = $request->input('school');
        $userdata->program = $request->input('program');
        $userdata->email = $request->input('email');
        $userdata->school_email = $request->input('school_email');
        $userdata->facebook_id = $request->input('facebook_id');
        $userdata->save();

        $ddata = [];

        if(isset($userdata))
        {
            $ddata = array(
                'id'=>$userdata->id,
                'name'=>$userdata->name,
                'email'=>$userdata->email,
                'facebook_id'=>$userdata->facebook_id,
                'school'=>$userdata->school,
                'school_email'=>$userdata->school_email,
                'program'=>$userdata->program
            );
        }

        $token = JWTAuth::fromUser($userdata);

        $data = ['message' => 'success', 'data' => $ddata,'token' =>  $token ];

        return $data;
    }

    public function userProfile(Request $request){

        $oldpassowrd = $request->input('oldpassword');

        $newpassword = $request->input('newpassword');

        $user_id = $request->input('user_id');
        if(isset($user_id)){
            $userdata=User::where('id','=',$user_id)->first();

//            if(!empty($request->input('email'))){
//                $useremail=User::where('id','=',$user_id)->where('email','=',$request->input('email'))->first();
//                if($useremail){
//                    $final = response()->json(['message' => 'failure','data' => 'Email Id already Exists!']);
//                    return $final;
//                }
//                $userdata->email = $request->input('email');
//             //   $userdata->school_email = $request->input('email');
//            }
            if(!empty($request->input('school'))){
                $userdata->school = $request->input('school');
            }
            if(!empty($request->input('program'))){
                $userdata->program = $request->input('program');
            }

            if($newpassword && $oldpassowrd) {

                $user_checking = User::where('id', '=', $user_id)->first();
                $hashedOldpassowrd = $user_checking->password;

                if (Hash::check($oldpassowrd, $hashedOldpassowrd)) {
                    $userdata->password = bcrypt($newpassword);
                    //  $final = response()->json(['message' => 'Password Changed Succesfully','data'=>$request->input('newpassword')]);
                    // return $final;
                } else {
                    $final = response()->json(['message' => 'failure','data' => 'Please Enter Correct old Password']);
                    return $final;
                }
            }

            $userdd = $userdata->save();

            if($userdata)
            {
                $final  =   response()->json(['message' => 'success','data'=>$userdata->getOriginal()]);
                return $final;
            }
            else
            {
                $final  =   response()->json(['message' => 'failure']);
                return $final;
            }
        }

    }

    public function forgetpassword(Request $request)
    {
        $email=$request->get('email');
        $userdata=User::where('email','=',$email)->first();
        if($userdata===null)
        {
            $final  =   response()->json(['message' => 'email is not exist']);
            return $final;
        }
        else
        {
            $rendomstrind=uniqid();
            Mail::raw('your new password is '.$rendomstrind, function($message) use ($email)
            {
                $message->to($email, 'User')->subject('New password');
                $message->from('testing.rvtech@gmail.com');
            });

            $userdata->password=Hash::make($rendomstrind);

            $userdata->save();

            if($userdata)
            {
                $final  =   response()->json(['message' => 'success','data'=>$rendomstrind]);
                return $final;
            }
            else
            {
                $final  =   response()->json(['message' => 'failure']);
                return $final;
            }
        }
    }


    public function changePassword(Request $request){
        $oldpassowrd = $request->input('oldpassword');

        $newpassword = $request->input('newpassword');
        $user_id = $request->input('user_id');

        if($user_id){

            if($newpassword && $oldpassowrd) {

                $userdata = User::where('id', '=', $user_id)->first();
                $hashedOldpassowrd = $userdata->password;

                if (Hash::check($oldpassowrd, $hashedOldpassowrd)) {
                    $userdata->password = bcrypt($newpassword);
                    $userdata->save();
                    $final = response()->json(['message' => 'Password Changed Succesfully','data'=>$request->input('newpassword')]);
                    return $final;
                } else {
                    $final = response()->json(['message' => 'Please Enter Correct old Password']);
                    return $final;
                }
            } else {
                $final  =   response()->json(['message' => 'Please Enter Correct Passwords']);
                return $final;
            }

        } else {
            $final  =   response()->json(['message' => 'Please Send User ID']);
            return $final;
        }
    }

    public function postfblogin(Request $request) {

        $rules = Module::validateRules("users", $request);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return json_encode(['message' => 'failure', 'data' => $validator->errors()->all()]);
        }
        $userdata = [];

        $userdata=User::where('email','=',$request->input('email'))->where('facebook_id','=',$request->input('facebook_id'))->first();

        if($userdata){
            $token = JWTAuth::fromUser($userdata);


            if($userdata->school){
                $school = DB::select(DB::raw('SELECT school_name FROM school_list where id ='.$userdata->school) );

                $userdata = $userdata->getOriginal();

                $userdata['school'] = $school[0]->school_name;
            }

            $final  =   response()->json(['message' => 'User Already Exists!','data'=>$userdata,'token'=>$token]);

            return $final;
        } else {

        }

    }

}
