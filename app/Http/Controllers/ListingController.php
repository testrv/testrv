<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use App\User;
use Mail;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use App\Models\Featured_Deal;
use App\Models\Deal_Report;
use App\Models\Bug_Report;
use App\Models\Notification_Setting;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\AuthenticateController;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\LA\UploadsController;
class ListingController extends Controller
{
    public $show_action = true;
    public $view_col = 'deal_order';
    public $listing_cols = ['id', 'deal_title', 'deal_name', 'deal_address', 'deal_phone', 'deal_price', 'deal_image', 'deal_description', 'deal_order', 'deal_archived'];

    public function __construct() {
        // Field Access of Listing Columns
        if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
            $this->middleware(function ($request, $next) {
                $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
                return $next($request);
            });
        } else {
            $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
        }
    }

    /**
     * Display a listing of the Featured_Deals.
     *
     * @return \Illuminate\Http\Response
     */

    public function pictureUrl($id){
        $upload = \App\Models\Upload::find($id);
        if(isset($upload->id)) {
            $value = url("files/".$upload->hash.DIRECTORY_SEPARATOR.$upload->name);
        } else {
            $value = 'Uplaoded file not found.';
        }
        return $value;
    }
    public function getData()
    {

        $values = DB::table('featured_deals')->whereNull('deleted_at')->get();
        $data = [];
        if(isset($values)){
            foreach ($values as $key=>$arValue){
                foreach ($arValue as $k=>$val){
                    if($k ==  'deal_image'){
                        $data[$key][$k] = $this->pictureUrl($val);
                    } else {
                        $data[$key][$k] = $val;
                    }
                }
            }
        }

        $data = ['message' => 'success', 'data' => $data ];

        return $data;

    }
    public function geoCodeLocator($address){
        $address = urlencode($address);
        $api='http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false';
        $geocode = file_get_contents($api);

        $output= json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;
        $latlong = [];
        $latlong['latitude'] = $latitude;
        $latlong['longitude'] = $longitude;
        return $latlong;
    }

    public function postDeal(UploadsController $uploadscontroller,Request $request)
    {

        $rules = Module::validateRules("Featured_Deals", $request);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $address = $request->input('deal_address'); // Google HQ
        $prepAddr = str_replace(' ','+',$address);
//        $latlong = $this->geoCodeLocator($prepAddr);
//        if(isset($latlong['latitude']) && isset($latlong['longitude'])){
//            $request['latitude'] = $latlong['latitude'];
//            $request['longitude'] = $latlong['longitude'];
//        }
        $image = $uploadscontroller->upload_files2($request['deal_image'],$request['user_id']);

        $dealsdata = new Featured_Deal();
        $dealsdata->deal_title=$request['deal_title'];
        $dealsdata->deal_name=$request['deal_name'];
        $dealsdata->user_id=$request['user_id'];
        $dealsdata->deal_address=$request['deal_address'];
        $dealsdata->deal_image=$image;
        $dealsdata->deal_description=$request['deal_description'];
        $dealsdata->deal_archived=$request['deal_archived'];
        $dealsdata->deal_type='Explore';
        $dealsdata->latitude=$request['deal_latitude'];
        $dealsdata->longitude=$request['deal_longitude'];
        $dealsdata->school_id=$request['school_id'];

        if($dealsdata->save()){
            $data = ['message' => 'success', 'data' => $dealsdata->getOriginal() ];
        } else {
            $data = ['message' => 'error', 'data' => 'Something went Wrong. Please contact us.' ];
        }
        return $data;

    }
    public function postNotification(Request $request){
        $data = $request->all();
        $notify=Notification_Setting::where('user_id','=',$data['user_id'])->first();

        if(!$notify){
            $notify = new Notification_Setting();
            $notify->user_id=$data['user_id'];
        }
        foreach ($data as $key=>$value){
            if($value != null){
                $notify->$key=$value;
            }
        }

        $notify->save();

        if($notify){
            $data = ['message' => 'success', 'data' => $notify->getOriginal() ];
        } else {
            $data = ['message' => 'error', 'data' => 'Something Went Wrong!' ];
        }
        return $data;

    }
    public function getNotification(Request $request){
        $data = $request->all();
        $notify=Notification_Setting::where('user_id','=',$data['user_id'])->first();
        if($notify){
            $data = ['message' => 'success', 'data' => $notify->getOriginal() ];
        } else {
            $data = ['message' => 'error', 'data' => 'Data Not Exist' ];
        }
        return $data;
    }
    public function reportBug(Request $request){

        $data = $request->all();
        $user = User::findOrFail($data['user_id']);
        $email = $user->email;

        Mail::send('emails.bugreport', ['user' => $user,'data'=>$data['message']], function ($message)  use ($email)
        {
            $message->to('nisha_shukla@rvtechnologies.co.in', 'User')->subject('New password');
            $message->from('testing.rvtech@gmail.com');
        });
        if (Mail::failures()) {
            $data = ['message' => 'error', 'data' => 'Mail not sent due to some technival issue' ];
            return $data;
        }

        $deal_data = new Bug_Report();
        if(isset($data['user_id'])){
            $deal_data->user_id = $data['user_id'];
        }
        if(isset($data['message'])){
            $deal_data->message = $data['message'];
        }
        if(isset($data['deal_id'])){
            $deal_data->deal_id = $data['deal_id'];
        }

        $deal_data->save();
        $data = ['message' => 'success', 'data' => 'Mail Sent Successfully' ];
        return $data;

    }
    public function dealReport(Request $request){

        $data = $request->all();
        $user = User::findOrFail($data['user_id']);
        $email = $user->email;

        Mail::send('emails.bugreport', ['user' => $user,'data'=>$data['message']], function ($message)  use ($email)
        {
            $message->to('testing.rvtech@gmail.com', 'User')->subject('New password');
            $message->from('test4rvtech@gmail.com');
            $message->subject('Deals Report');
        });
        if (Mail::failures()) {
            $data = ['message' => 'error', 'data' => 'Mail not sent due to some technival issue' ];
            return $data;
        }
        $deal_data = new Deal_Report();
        if(isset($data['user_id'])){
            $deal_data->user_id = $data['user_id'];
        }
        if(isset($data['message'])){
            $deal_data->message = $data['message'];
        }
        if(isset($data['type'])){
            $deal_data->type = $data['type'];
        }
        if(isset($data['deal_id'])){
            $deal_data->deal_id = $data['deal_id'];
        }
       
        $deal_data->save();

        $data = ['message' => 'success', 'data' => 'Mail Sent Successfully' ];
        return $data;

    }

    public function getSearch(Request $request)
    {
        $data = $request->all();

        $search_value = $data['key'];

        $columns = ['deal_title'=>$search_value,'deal_name'=>$search_value,'deal_address'=>$search_value,'deal_phone'=>$search_value,'deal_price'=>$search_value,'deal_description'=>$search_value,'deal_order'=>$search_value,'deal_archived'=>$search_value];
        DB::EnableQueryLog();
        $query = Featured_Deal::select('*');

//        foreach($columns as $column)
//        {
//            $query->orwhere($column, 'like', "%".$search_value."%");
//        }

        $query = $query->whereNull('deleted_at')->where(function($query) use($columns){
            foreach($columns as $key =>$value){
                $query->orwhere($key ,'like','%'.$value.'%');
            }
        });
        $query = $query->paginate(15);

       // dd(DB::GetQueryLog());
        if($query){
            $values =  $query->toArray();
        }

       // dd($query->count());
       //dd(DB::GetQueryLog());
        //$values = DB::table('featured_deals')->whereNull('deleted_at')->where('deal_title','like','%"'.$data['key'].'"%')->get();

        $data = [];

        if(isset($values['data'])){
            foreach ($values['data'] as $key=>$arValue){

                foreach ($arValue as $k=>$val){
                    if($k ==  'deal_image'){
                        $data[$key][$k] = $this->pictureUrl($val);
                    } else {
                        $data[$key][$k] = $val;
                    }
                }
            }
        }

        unset($values['data']);
        $values['data'] = $data;
        $data = ['message' => 'success', 'data' => $values ];

        return $data;

    }

    public function schoollist(){
           $results = DB::select(DB::raw('SELECT * FROM school_list') );
           $data = ['message' => 'success', 'data' => $results ];
           return $data;
    }

    public function nearestLocation(Request  $request){

        $data = $request->all();
        $search_value = '';
        if(isset($data['key'])){
            $search_value = $data['key'];
        }
        DB::EnableQueryLog();
        if($search_value){
            $columns = ['deal_title'=>$search_value,'deal_name'=>$search_value,'deal_address'=>$search_value,'deal_phone'=>$search_value,'deal_price'=>$search_value,'deal_description'=>$search_value,'deal_order'=>$search_value,'deal_archived'=>$search_value];

            $schoolid = $data['school_id'];
            $results=Featured_Deal::whereNull('deleted_at')->where(function($query) use ($schoolid) {
                $query->where('school_id','=',$schoolid)
                    ->orwhere('deal_type','=','featured');
            })
                ->where(function($query) use($columns){
                foreach($columns as $key =>$value){
                    $query->orwhere($key ,'like','%'.$value.'%');
                }
                })->select('featured_deals.*')
                ->selectRaw('( 6371 * acos( cos( radians(?) ) *
                              cos( radians( latitude ) )
                              * cos( radians( longitude ) - radians(?)
                              ) + sin( radians(?) ) *
                              sin( radians( latitude ) ) )
                            ) AS distance', [$data['lat'], $data['long'], $data['lat']])
                ->orderBy('distance','ASC')->paginate(5);

            if($results){
                $results =  $results->toArray();
            }
            //dd( DB::GetQueryLog());
        } else {

           // $results = DB::select(DB::raw('SELECT *, ( 6371 * acos( cos( radians(' . $data['lat'] . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $data['long'] . ') ) + sin( radians(' . $data['lat'] .') ) * sin( radians(latitude) ) ) ) AS distance FROM featured_deals HAVING distance < 3 and deleted_at is NULL ORDER BY distance,deal_order') );


            $results=Featured_Deal::whereNull('deleted_at')->where('school_id','=',$data['school_id'])->orwhere('deal_type','=','featured')
            ->select('featured_deals.*')
                ->selectRaw('( 6371 * acos( cos( radians(?) ) *
                              cos( radians( latitude ) )
                              * cos( radians( longitude ) - radians(?)
                              ) + sin( radians(?) ) *
                              sin( radians( latitude ) ) )
                            ) AS distance', [$data['lat'], $data['long'], $data['lat']])
                ->orderBy('distance','ASC')->paginate(5);

            if($results){
                $results =  $results->toArray();
            }
        }
       // dd(DB::GetQueryLog());

        $data = [];

        if(isset($results['data'])){
            foreach ($results['data'] as $key=>$arValue){
                foreach ($arValue as $k=>$val){
                    if($k ==  'deal_image'){
                        $data[$key][$k] = $this->pictureUrl($val);
                    } else {
                        $data[$key][$k] = $val;
                    }
                }
            }
        }

        unset($results['data']);

        $results['data'] = $data;


        $data = ['message' => 'success', 'data' => $results ];

        return $data;


    }

    public function fcmupdate(Request  $request)
    {
        $data = $request->all();

        $results = User::where('fcm_token','=',$data['fcm_token'])->first();

        if($results){
            User::where('id','=',$results->id)->update(['fcm_token' => null]);
        }

        User::where('id','=',$data['user_id'])->update(['fcm_token' => $data['fcm_token']]);
        $data = ['message' => 'success', 'data' => $results ];
        return $data;
    }

    public function pushnotification($message,$fcm_token,$scheduledata)
    {

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array (
            'registration_ids' => $fcm_token,
            'notification' => array (
                "body" => $message,
                "scheduledata"=>$scheduledata,
            )
        );
        $fields = json_encode ( $fields );

        $headers = array (
            'Authorization: key=' . "AAAApT6cWZM:APA91bGxrHCh5Vr4kZjrHyyphjzSZuj7oo55vf5-2OCCqFx5fQD_AIFow_m39FMaCOU9ixVAfSlSVc8ztdOOBRySJhn9Oc0V4CgqO07GvIJ5-BqwjQApl33nmAYoEjp-1KjQrghaAJr4",
            'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        curl_close ( $ch );
        return $result;

    }


}
