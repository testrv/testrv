<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Explore_Deal;
use App\Models\Featured_Deal;

class Explore_DealsController extends Controller
{
    public $show_action = true;
    public $view_col = 'deal_order';
    public $listing_cols = ['id', 'deal_title', 'deal_name', 'deal_address', 'deal_phone', 'deal_price', 'deal_image', 'deal_description', 'deal_order', 'deal_archived'];

    public function __construct() {
        // Field Access of Listing Columns
        if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
            $this->middleware(function ($request, $next) {
                $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
                return $next($request);
            });
        } else {
            $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
        }
    }


    public function index()
    {
        $module = Module::get('Featured_Deals');
        $column_name = array('id','deal_title','deal_order');
        if(Module::hasAccess($module->id)) {
            return View('la.explore_deals.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => $column_name,
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Show the form for creating a new featured_deal.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created featured_deal in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("Featured_Deals", "create")) {

            $rules = Module::validateRules("Featured_Deals", $request);

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $insert_id = Module::insert("Featured_Deals", $request);

            return redirect()->route(config('laraadmin.adminRoute') . '.explore_deals.index');

        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Display the specified featured_deal.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Module::hasAccess("Featured_Deals", "view")) {

            $featured_deal = Featured_Deal::find($id);
            if(isset($featured_deal->id)) {
                $module = Module::get('Featured_Deals');
                $module->row = $featured_deal;

                return view('la.explore_deals.show', [
                    'module' => $module,
                    'view_col' => $this->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('featured_deal', $featured_deal);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("explore_deals"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Show the form for editing the specified featured_deal.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Module::hasAccess("Featured_Deals", "edit")) {
            $featured_deal = Featured_Deal::find($id);
            if(isset($featured_deal->id)) {
                $module = Module::get('Featured_Deals');

                $module->row = $featured_deal;

                return view('la.explore_deals.edit', [
                    'module' => $module,
                    'view_col' => $this->view_col,
                ])->with('explore_deals', $featured_deal);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("explore_deals"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Update the specified featured_deal in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Module::hasAccess("Featured_Deals", "edit")) {

            $rules = Module::validateRules("Featured_Deals", $request, true);

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();;
            }

            $insert_id = Module::updateRow("Featured_Deals", $request, $id);

            return redirect()->route(config('laraadmin.adminRoute') . '.explore_deals.index');

        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Remove the specified featured_deal from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Module::hasAccess("Featured_Deals", "delete")) {
            Featured_Deal::find($id)->delete();

            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.explore_deals.index');
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Datatable Ajax fetch
     *
     * @return
     */
    public function dtajax()
    {
        //dd($this->listing_cols);
        $column_name = array('id','deal_title','deal_order');
        $values = DB::table('featured_deals')->select($column_name)->whereNull('deleted_at')->where('deal_type','=','explore');

        $out = Datatables::of($values)->make();

        $data = $out->getData();

        $fields_popup = ModuleFields::getModuleFields('Featured_Deals');

        for($i=0; $i < count($data->data); $i++) {
            for ($j=0; $j < count($column_name); $j++) {

                $col = $this->listing_cols[$j];

                if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {

                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $this->view_col) {
                    $data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/explore_deals/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
                }

                // else if($col == "author") {
                //    $data->data[$i][$j];
                // }
            }

            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("Featured_Deals", "view")) {
                    $output .= '<a href="'.url(config('laraadmin.adminRoute') . '/explore_deals/'.$data->data[$i][0].'/').'" class="btn btn-view btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-eye"></i></a>';
                }
//                if(Module::hasAccess("Featured_Deals", "edit")) {
//                    $output .= '<a href="'.url(config('laraadmin.adminRoute') . '/explore_deals/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
//                }

                if(Module::hasAccess("Featured_Deals", "delete")) {
                    $output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.explore_deals.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
                    $output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
                    $output .= Form::close();
                }
                $data->data[$i][] = (string)$output;
            }
        }

        $out->setData($data);
        return $out;
    }

}
