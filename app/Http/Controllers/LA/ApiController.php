<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Featured_Deal;
use App\Models\Featured_Deal_Api;
use App\Models\Deal_User;

class ApiController extends Controller
{
    public $show_action = true;
    public $view_col = 'deal_order';
    public $listing_cols = ['id', 'deal_title', 'deal_name', 'deal_address', 'deal_phone', 'deal_price', 'deal_image', 'deal_description', 'deal_order', 'deal_archived'];

    public function __construct() {
        // Field Access of Listing Columns
        if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
            $this->middleware(function ($request, $next) {
                $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
                return $next($request);
            });
        } else {
            $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
        }
    }
    public function signup(Request $request){
        if($request->all()) {

            $rules = Module::validateRules("Deal_Users", $request);

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $data = array(
                    'insert_id'=>'',
                    'data'=>'',
                    'success'=>'fail'
                );
                return $data;
            }

            $insert_id = Module::insert("Deal_Users", $request);
            $deal_user = Deal_User::find($insert_id)->get();
            $data = array(
                'insert_id'=>$insert_id,
                'data'=>$deal_user,
                'success'=>'true'
            );
            $customClaims = [
                'sub' => $insert_id,
                'iat' => time(),
                'exp' => time() + (2 * 7 * 24 * 60 * 60)
            ];
            $payload = app('tymon.jwt.payload.factory')->make($customClaims);
            return JWTAuth::encode($payload, Config::get('app.token_secret'));

//            $token = JWTAuth::fromUser('');
//            dd($data);
            return Response::json(compact('token'));

        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }
//    public function authenticate()
//    {
//        $credentials = Input::only('email', 'password');
//
//        try {
//            $user = User::create($credentials);
//        } catch (Exception $e) {
//            return Response::json(['error' => 'User already exists.'], HttpResponse::HTTP_CONFLICT);
//        }
//
//        $token = JWTAuth::fromUser($user);
//
//        return Response::json(compact('token'));
//    }

}
