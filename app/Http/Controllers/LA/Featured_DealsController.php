<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Mail;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use App\Models\Featured_Deal;
use App\Models\Deal_Report;
use App\Models\Bug_Report;
use App\User;
class Featured_DealsController extends Controller
{
    public $show_action = true;
    public $view_col = 'deal_order';
    public $listing_cols = ['id', 'deal_title', 'deal_name', 'deal_address', 'deal_phone', 'deal_price', 'deal_image', 'deal_description', 'deal_order', 'deal_archived'];

    public function __construct() {
        // Field Access of Listing Columns
        if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
            $this->middleware(function ($request, $next) {
                $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
                return $next($request);
            });
        } else {
            $this->listing_cols = ModuleFields::listingColumnAccessScan('Featured_Deals', $this->listing_cols);
        }
    }

    /**
     * Display a listing of the Featured_Deals.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $module = Module::get('Featured_Deals');
        $column_name = array('id','deal_title','deal_order');
        if(Module::hasAccess($module->id)) {
            return View('la.featured_deals.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => $column_name,
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Show the form for creating a new featured_deal.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created featured_deal in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("Featured_Deals", "create")) {

            $rules = Module::validateRules("Featured_Deals", $request);

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }


            $address = $request->input('deal_address'); // Google HQ
            $prepAddr = str_replace(' ','+',$address);
            $latitude = '';
            $longitude = '';
            if($prepAddr){
                $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
                $output= json_decode($geocode);
                $latitude = $output->results[0]->geometry->location->lat;
                $longitude = $output->results[0]->geometry->location->lng;
            }


            $request['latitude'] = $latitude;
            $request['longitude'] = $longitude;


            $dealsdata = new Featured_Deal();
            $dealsdata->deal_title=$request['deal_title'];
            $dealsdata->deal_name=$request['deal_name'];
            $dealsdata->deal_address=$request['deal_address'];
            $dealsdata->deal_phone=$request['deal_phone'];
            $dealsdata->deal_price=$request['deal_price'];
            $dealsdata->deal_image=$request['deal_image'];
            $dealsdata->deal_description=$request['deal_description'];
            $dealsdata->deal_order=$request['deal_order'];
            $dealsdata->deal_archived=$request['deal_archived'];
            $dealsdata->deal_type=$request['deal_type'];
            $dealsdata->latitude=$request['latitude'];
            $dealsdata->longitude=$request['longitude'];
            $dealsdata->user_id= Auth::user()->id;
            $dealsdata->save();

//            $insert_id = Module::insert("Featured_Deals", $request);


            return redirect()->route(config('laraadmin.adminRoute') . '.featured_deals.index');

        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Display the specified featured_deal.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Module::hasAccess("Featured_Deals", "view")) {

            $featured_deal = Featured_Deal::find($id);
            if(isset($featured_deal->id)) {
                $module = Module::get('Featured_Deals');
                $module->row = $featured_deal;

                return view('la.featured_deals.show', [
                    'module' => $module,
                    'view_col' => $this->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('featured_deal', $featured_deal);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("featured_deal"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Show the form for editing the specified featured_deal.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Module::hasAccess("Featured_Deals", "edit")) {
            $featured_deal = Featured_Deal::find($id);
            if(isset($featured_deal->id)) {
                $module = Module::get('Featured_Deals');

                $module->row = $featured_deal;

                return view('la.featured_deals.edit', [
                    'module' => $module,
                    'view_col' => $this->view_col,
                ])->with('featured_deal', $featured_deal);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("featured_deal"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Update the specified featured_deal in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Module::hasAccess("Featured_Deals", "edit")) {

            $rules = Module::validateRules("Featured_Deals", $request, true);

            $dealsdata=Featured_Deal::where('id','=',$id)->first();

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();;
            }

            $address = $request->input('deal_address'); // Google HQ
            $prepAddr = str_replace(' ','+',$address);
            $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
            $output= json_decode($geocode);
            $latitude = $output->results[0]->geometry->location->lat;
            $longitude = $output->results[0]->geometry->location->lng;

            $request['latitude'] = $latitude;
            $request['longitude'] = $longitude;

//
            $dealsdata->deal_title=$request['deal_title'];
            $dealsdata->deal_name=$request['deal_name'];
            $dealsdata->deal_address=$request['deal_address'];
            $dealsdata->deal_phone=$request['deal_phone'];
            $dealsdata->deal_price=$request['deal_price'];
            $dealsdata->deal_image=$request['deal_image'];
            $dealsdata->deal_description=$request['deal_description'];
            $dealsdata->deal_order=$request['deal_order'];
            $dealsdata->deal_archived=$request['deal_archived'];
            $dealsdata->deal_type=$request['deal_type'];
            $dealsdata->latitude=$request['latitude'];
            $dealsdata->longitude=$request['longitude'];
            $dealsdata->user_id= Auth::user()->id;
            $dealsdata->save();

//            $insert_id = Module::updateRow("Featured_Deals", $request, $id);

            return redirect()->route(config('laraadmin.adminRoute') . '.featured_deals.index');

        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Remove the specified featured_deal from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Module::hasAccess("Featured_Deals", "delete")) {
            Featured_Deal::find($id)->delete();

            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.featured_deals.index');
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /**
     * Datatable Ajax fetch
     *
     * @return
     */
    public function dtajax()
    {
        //dd($this->listing_cols);
        $column_name = array('id','deal_title','deal_order');
        $values = DB::table('featured_deals')->select($column_name)->whereNull('deleted_at')->where('deal_type','=','featured');

        $out = Datatables::of($values)->make();

        $data = $out->getData();

        $fields_popup = ModuleFields::getModuleFields('Featured_Deals');

        for($i=0; $i < count($data->data); $i++) {
            for ($j=0; $j < count($column_name); $j++) {

                $col = $this->listing_cols[$j];

                if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {

                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $this->view_col) {
                    $data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/featured_deals/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
                }

                // else if($col == "author") {
                //    $data->data[$i][$j];
                // }
            }

            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("Featured_Deals", "view")) {
                    $output .= '<a href="'.url(config('laraadmin.adminRoute') . '/featured_deals/'.$data->data[$i][0].'/').'" class="btn btn-view btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-eye"></i></a>';
                }
                if(Module::hasAccess("Featured_Deals", "edit")) {
                    $output .= '<a href="'.url(config('laraadmin.adminRoute') . '/featured_deals/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
                }

                if(Module::hasAccess("Featured_Deals", "delete")) {
                    $output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.featured_deals.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
                    $output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
                    $output .= Form::close();
                }
                $data->data[$i][] = (string)$output;
            }
        }

        $out->setData($data);
        return $out;
    }
    public function notificationlisting(Request $request){
        $module = Module::get('Featured_Deals');
        $column_name = array('id','deal_title','deal_type');
        if(Module::hasAccess($module->id)) {
            return view('la.featured_deals.notificationlisting', [
                'show_actions' => $this->show_action,
                'listing_cols' => $column_name,
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }

    }

    public function notificationdtajax()
    {
        //dd($this->listing_cols);
        $column_name = array('id','deal_title','deal_type');
        $values = DB::table('featured_deals')->select($column_name)->whereNull('deleted_at');

        $out = Datatables::of($values)->make();

        $data = $out->getData();

        $fields_popup = ModuleFields::getModuleFields('Featured_Deals');

        for($i=0; $i < count($data->data); $i++) {
            for ($j=0; $j < count($column_name); $j++) {

                $col = $this->listing_cols[$j];

                if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {

                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }

            }
            if(Module::hasAccess("Featured_Deals", "view")) {
                $output = '';
                $output .= '<a href="'.url(config('laraadmin.adminRoute') . '/pushnotification/'.$data->data[$i][0].'/').'" class="btn btn-view btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-envelope"></i> Send Notification</a>';
                $data->data[$i][] = (string)$output;
            }

        }

        $out->setData($data);
        return $out;
    }

    public function pushnotification($id){

        if(Module::hasAccess("Featured_Deals", "view")) {

            $featured_deal = Featured_Deal::find($id);
            if(isset($featured_deal->id)) {
                $module = Module::get('Featured_Deals');
                $module->row = $featured_deal;

                return view('la.featured_deals.notificationpush', [
                    'module' => $module,
                    'record_id' => $id,
                    'view_col' => $this->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('featured_deal', $featured_deal);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("featured_deal"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    public function autosuggestuser(Request $request){
        $keyword = $request->input('term');

        $column_name = array('id','name','email');
        $data = [];
        $values = DB::table('users')->select($column_name)->whereNull('deleted_at')->where('name','like','%'.$keyword.'%')->get();
        if(isset($values)){
            foreach ($values as $key=>$v){
                $data[$key] = array('id' => $v->id,'label'=>$v->name,'value'=>$v->email);
            }
        }
        return $data;
    }
    public function notificationsent(Request $request)
    {
        if (Module::hasAccess("Featured_Deals", "view")) {

            $data = $request->all();
            $id = $request->input('recordid');
            $sendto = explode(',',$data['sendto']);
            $sendto = array_filter($sendto);
            foreach($sendto as $key=>$value){
                $mailids[]= trim($value);
            }
            DB::EnableQueryLog();

            $push_notif = User::whereIn('email',$mailids)->join('notification_settings', 'users.id', '=', 'notification_settings.user_id')->where('push_notification','=',1)->get();
            $email_notif = User::whereIn('email',$mailids)->join('notification_settings', 'users.id', '=', 'notification_settings.user_id')->where('email_notification','=',1)->get();



            try {
                if(isset($push_notif)){
                    $push_notif = $push_notif->toArray();
                    $fcm_token = [];
                    foreach($push_notif as $key=>$value){
                        if(trim($value['fcm_token'])){
                            $fcm_token[]=$value['fcm_token'];
                        }
                    }

                    $push_notifdata = app('App\Http\Controllers\ListingController')->pushnotification($data['pushmessage'],$fcm_token,1);

                }
                $emaildata = [];
                if(isset($email_notif)){
                    $email_notif = $email_notif->toArray();
                    foreach($email_notif as $key=>$value){
                        if(trim($value['email'])){
                            $emailids[]=array('email'=>$value['email'],'name'=>$value['name']);
                        }
                    }

                    $messg = $data['pushmessage'];
                    if(isset($emailids)){
                        foreach($emailids as $key=>$value){
                            $email = $value['email'];

                            Mail::send('emails.emailnotification', ['emaildata'=>$value['name'],'data'=>$messg], function ($message)  use ($email)
                            {
                                $message->to($email, 'User')->subject('Notification from Scavenger.');
                                $message->from('testing.rvtech@gmail.com');
                            });
                            if (Mail::failures()) {
                                $data = ['message' => 'error', 'data' => 'Mail not sent due to some technical issue' ];
                                $emaildata['error'] = $value['email'];
                            } else {
                                $emaildata['success'] = $value['email'];
                            }

                        }
                    }


                }
            }
            catch (\Exception $e) {
                return $e->getMessage();
            }

            return redirect()->back()->with('message', $emaildata);
           // return redirect(config('laraadmin.adminRoute') . "/pushnotification/" . $id);
        }
    }

    /****Deals Report****/
    public function dealdestroy($id)
    {
        if(Module::hasAccess("Featured_Deals", "delete")) {
            Deal_Report::find($id)->delete();

            // Redirecting to index() method
            return redirect(config('laraadmin.adminRoute')."/dealsreport");
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    public function dealsreport(){
        $module = Module::get('Featured_Deals');
        $column_name = array('Deal ID','Type of Report','Deal ID','Deal Title','Deal Type','Submitted By');
        if(Module::hasAccess($module->id)) {
            return view('la.featured_deals.dealsreport', [
                'show_actions' => $this->show_action,
                'listing_cols' => $column_name,
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    public function dealsreport_dtajax()
    {
        //dd($this->listing_cols);
        $column_name = array('Deal ID','Type of Report','Deal ID','Deal Title','Deal Type','Submitted By');
//        $values = DB::table('deal_reports')->whereNull('deleted_at');

        DB::EnableQueryLog();
        $values = DB::table('deal_reports')->select('deal_reports.id','deal_reports.type','deal_reports.deal_id','featured_deals.deal_title','featured_deals.deal_type','users.name')->leftJoin('users', function($join) {
            $join->on('users.id', '=', 'deal_reports.user_id');
        })->leftJoin('featured_deals', function($join) {
            $join->on('featured_deals.id', '=', 'deal_reports.deal_id');
        })->whereNull('deal_reports.deleted_at');



        $out = Datatables::of($values)->make();

        $data = $out->getData();

        $fields_popup = ModuleFields::getModuleFields('Featured_Deals');

        for($i=0; $i < count($data->data); $i++) {
            for ($j=0; $j < count($column_name); $j++) {

                $col = $this->listing_cols[$j];

                if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {

                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $this->view_col) {
                    $data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/dealsreport/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
                }

            }

            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("Featured_Deals", "view")) {
                    $output .= '<a href="'.url(config('laraadmin.adminRoute') . '/dealReportshow/'.$data->data[$i][0].'/').'" class="btn btn-view btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-eye"></i></a>';
                }
                if(Module::hasAccess("Featured_Deals", "delete")) {
                    $output .= '<a href="'.url(config('laraadmin.adminRoute') . '/dealdestroy/'.$data->data[$i][0].'/').'" class="btn btn-view btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-times"></i></a>';
                }
                $data->data[$i][] = (string)$output;
            }
        }

        $out->setData($data);
        return $out;
    }
    public function dealReportshow($id)
    {
        if(Module::hasAccess("Featured_Deals", "view")) {

            $module = Deal_Report::find($id);
            if(isset($id)) {
              //  $module = Module::get('Featured_Deals');
                $deal_report = DB::table('deal_reports')->select('deal_reports.id as dealid' ,'deal_reports.deal_id','deal_reports.type','users.name','deal_reports.message','featured_deals.id','featured_deals.deal_title','featured_deals.deal_type')->leftJoin('users', function($join) {
                    $join->on('users.id', '=', 'deal_reports.user_id');
                })->
                leftJoin('featured_deals', function($join) {
                    $join->on('featured_deals.id', '=', 'deal_reports.deal_id');
                })->whereNull('deal_reports.deleted_at')->where('deal_reports.id','=',$id)->first();


                $module->row = $deal_report;

                return view('la.featured_deals.dealreportshow', [
                    'module' => $module,
                    'view_col' => $this->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('deal_report', $deal_report);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("featured_deal"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    /****Bug Report****/
    public function bugdestroy($id)
    {
        if(Module::hasAccess("Featured_Deals", "delete")) {
            Bug_Report::find($id)->delete();

            // Redirecting to index() method
            return redirect(config('laraadmin.adminRoute')."/bugreport");
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }
    public function bugreport(){
        $module = Module::get('Featured_Deals');
        $column_name = array('Bug ID','Deal ID','Deal Title','Deal Type','Submitted By');
        if(Module::hasAccess($module->id)) {
            return view('la.featured_deals.bugreport', [
                'show_actions' => $this->show_action,
                'listing_cols' => $column_name,
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }

    public function bugreport_dtajax()
    {
        //dd($this->listing_cols);
        $column_name = array('Bug ID','Deal ID','Deal Title','Deal Type','Submitted By');
//        $values = DB::table('deal_reports')->whereNull('deleted_at');

        DB::EnableQueryLog();
        $values = DB::table('bug_report')->select('bug_report.id','bug_report.deal_id','featured_deals.deal_title','featured_deals.deal_type','users.name')->leftJoin('users', function($join) {
            $join->on('users.id', '=', 'bug_report.user_id');
        })->leftJoin('featured_deals', function($join) {
            $join->on('featured_deals.id', '=', 'bug_report.deal_id');
        })->whereNull('bug_report.deleted_at');



        $out = Datatables::of($values)->make();

        $data = $out->getData();

        $fields_popup = ModuleFields::getModuleFields('Featured_Deals');

        for($i=0; $i < count($data->data); $i++) {
            for ($j=0; $j < count($column_name); $j++) {

                $col = $this->listing_cols[$j];

                if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {

                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $this->view_col) {
                    $data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/bugreport/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
                }


            }

            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("Featured_Deals", "view")) {
                    $output .= '<a href="'.url(config('laraadmin.adminRoute') . '/bugReportshow/'.$data->data[$i][0].'/').'" class="btn btn-view btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-eye"></i></a>';
                }
                if(Module::hasAccess("Featured_Deals", "delete")) {
                    $output .= '<a href="'.url(config('laraadmin.adminRoute') . '/bugdestroy/'.$data->data[$i][0].'/').'" class="btn btn-view btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-times"></i></a>';
                }
                $data->data[$i][] = (string)$output;
            }
        }

        $out->setData($data);
        return $out;
    }
    public function bugReportshow($id)
    {
        if(Module::hasAccess("Featured_Deals", "view")) {

            $module = Bug_Report::find($id);
            if(isset($id)) {
                //  $module = Module::get('Featured_Deals');
                $deal_report = DB::table('bug_report')->select('bug_report.id','bug_report.deal_id','featured_deals.deal_title','featured_deals.deal_type','users.name','bug_report.message')->leftJoin('users', function($join) {
                    $join->on('users.id', '=', 'bug_report.user_id');
                })->
                leftJoin('featured_deals', function($join) {
                    $join->on('featured_deals.id', '=', 'bug_report.deal_id');
                })->whereNull('bug_report.deleted_at')->where('bug_report.id','=',$id)->first();

                $module->row = $deal_report;

                return view('la.featured_deals.bugreportshow', [
                    'module' => $module,
                    'view_col' => $this->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('deal_report', $deal_report);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("featured_deal"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
    }
}
