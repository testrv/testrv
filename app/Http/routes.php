<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
   return view('login');
});
Route::post('/saveUser', 'LA\Deal_UsersController@saveUser');
/* ================== Homepage + Admin Routes ================== */

require __DIR__.'/admin_routes.php';

//Route::group(['middleware' => ['api','cors'],'prefix' => 'api'], function () {
//    Route::post('register', 'APIController@register');
//    Route::post('login', 'APIController@login');
//    Route::group(['middleware' => 'jwt-auth'], function () {
//        Route::post('get_user_details', 'APIController@get_user_details');
//    });
//});

Route::post('auth/register', 'UserController@postRegister');
Route::post('auth/fbregister', 'UserController@postSociallogin');
Route::post('auth/sociallogin','UserController@sociallogin');
Route::post('auth/login','UserController@login');
Route::post('auth/forgetpassword','UserController@forgetpassword');
Route::get('auth/getData','ListingController@getData');
Route::post('auth/postDeal','ListingController@postDeal');
Route::post('auth/userProfile','UserController@userProfile');
Route::post('auth/changePassword','UserController@changePassword');
Route::post('auth/postfblogin','UserController@postfblogin');
Route::post('auth/postNotification','ListingController@postNotification');
Route::get('auth/getNotification','ListingController@getNotification');
Route::post('auth/reportBug','ListingController@reportBug');
Route::post('auth/dealReport','ListingController@dealReport');
Route::get('auth/getSearch','ListingController@getSearch');
Route::get('auth/nearestLocation','ListingController@nearestLocation');
Route::get('auth/schoollist','ListingController@schoollist');
Route::post('auth/fcmupdate','ListingController@fcmupdate');


