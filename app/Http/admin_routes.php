<?php

/* ================== Homepage ================== */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');

}

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');


	/* ================== Featured_Deals ================== */
	Route::resource(config('laraadmin.adminRoute') . '/featured_deals', 'LA\Featured_DealsController');
	Route::get(config('laraadmin.adminRoute') . '/featured_deal_dt_ajax', 'LA\Featured_DealsController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/notificationdtajax', 'LA\Featured_DealsController@notificationdtajax');
	Route::get(config('laraadmin.adminRoute') . '/notificationlisting', 'LA\Featured_DealsController@notificationlisting');
	Route::get(config('laraadmin.adminRoute') . '/pushnotification/{id}', 'LA\Featured_DealsController@pushnotification');
	Route::get(config('laraadmin.adminRoute') . '/dealsreport', 'LA\Featured_DealsController@dealsreport');
	Route::get(config('laraadmin.adminRoute') . '/dealReportshow/{id}', 'LA\Featured_DealsController@dealReportshow');
    Route::get(config('laraadmin.adminRoute') . '/autosuggestuser', 'LA\Featured_DealsController@autosuggestuser');
    Route::post(config('laraadmin.adminRoute') . '/notificationsent', 'LA\Featured_DealsController@notificationsent');
    Route::get(config('laraadmin.adminRoute') . '/dealsreport_dtajax', 'LA\Featured_DealsController@dealsreport_dtajax');
    Route::get(config('laraadmin.adminRoute') . '/bugreport', 'LA\Featured_DealsController@bugreport');
    Route::get(config('laraadmin.adminRoute') . '/bugReportshow/{id}', 'LA\Featured_DealsController@bugReportshow');
    Route::get(config('laraadmin.adminRoute') . '/bugreport_dtajax', 'LA\Featured_DealsController@bugreport_dtajax');
    Route::get(config('laraadmin.adminRoute') . '/dealdestroy/{id}', 'LA\Featured_DealsController@dealdestroy');
    Route::get(config('laraadmin.adminRoute') . '/bugdestroy/{id}', 'LA\Featured_DealsController@bugdestroy');


	/* ================== Deal_Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/deal_users', 'LA\Deal_UsersController');
	Route::get(config('laraadmin.adminRoute') . '/deal_user_dt_ajax', 'LA\Deal_UsersController@dtajax');

	/* ================== Explore_Deals ================== */
	Route::resource(config('laraadmin.adminRoute') . '/explore_deals', 'LA\Explore_DealsController');
	Route::get(config('laraadmin.adminRoute') . '/explore_deal_dt_ajax', 'LA\Explore_DealsController@dtajax');
});
Route::post('/signup', 'LA\ApiController@signup');