@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/deal_users') }}">Deal User</a> :
@endsection
@section("contentheader_description", $deal_user->$view_col)
@section("section", "Deal Users")
@section("section_url", url(config('laraadmin.adminRoute') . '/deal_users'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Deal Users Edit : ".$deal_user->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($deal_user, ['route' => [config('laraadmin.adminRoute') . '.deal_users.update', $deal_user->id ], 'method'=>'PUT', 'id' => 'deal_user-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'username')
					@la_input($module, 'password')
					@la_input($module, 'confirm_password')
					@la_input($module, 'name')
					@la_input($module, 'school')
					@la_input($module, 'school_email')
					@la_input($module, 'program')
					@la_input($module, 'facebook_id')
					@la_input($module, 'email')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/deal_users') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#deal_user-edit-form").validate({
		
	});
});
</script>
@endpush
