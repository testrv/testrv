@extends('la.layouts.app')

@section('htmlheader_title')
    Featured Deal View
@endsection


@section('main-content')


    <div id="page-content" class="profile2">
        <ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
            <li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/notificationlisting') }}" data-toggle="tooltip" data-placement="right" title="Back to Featured Deals"><i class="fa fa-chevron-left"></i></a></li>
            <li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active fade in" id="tab-info">
                <div class="tab-content">
                    <div class="panel infolist">
                        <div class="panel-default panel-heading">
                            <h4>General Info</h4>
                        </div>
                        <div class="panel-body">

                            <form name="pushmail" method="post" action="<?php echo url(config('laraadmin.adminRoute') . '/notificationsent'); ?>" id="pushmail">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="email">Push Message:</label>
                                    <textarea name="pushmessage" class="form-control" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Send To:</label>
                                    {{--<input id="myAutocomplete" type="text" required name="sendto" id="sendto" />--}}
                                    <div class="ui-widget">
                                        <input type="text" required name="sendto" id="sendto" size="50" class="form-control">
                                    </div>
                                </div>
                                <input type="hidden" value="{{$record_id}}" name="recordid">
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </div>
                            </form>
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    <?php
                                        $notif_data =  session()->get('message');
                                    ?>
                                    Notification Sent Succesfully
                                    @if(isset($notif_data['error']))
                                        @foreach($notif_data['error'] as $key=>$value)
                                            <span class="err-mail">{{$value}}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-timeline">
                <ul class="timeline timeline-inverse">
                    <!-- timeline time label -->
                    <li class="time-label">
					<span class="bg-red">
						10 Feb. 2014
					</span>
                    </li>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-envelope bg-blue"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                            <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                            <div class="timeline-body">
                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                quora plaxo ideeli hulu weebly balihoo...
                            </div>
                            <div class="timeline-footer">
                                <a class="btn btn-primary btn-xs">Read more</a>
                                <a class="btn btn-danger btn-xs">Delete</a>
                            </div>
                        </div>
                    </li>
                    <!-- END timeline item -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-user bg-aqua"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                            <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
                            </h3>
                        </div>
                    </li>
                    <!-- END timeline item -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-comments bg-yellow"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                            <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                            <div class="timeline-body">
                                Take me to your leader!
                                Switzerland is small and neutral!
                                We are more like Germany, ambitious and misunderstood!
                            </div>
                            <div class="timeline-footer">
                                <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                            </div>
                        </div>
                    </li>
                    <!-- END timeline item -->
                    <!-- timeline time label -->
                    <li class="time-label">
					<span class="bg-green">
						3 Jan. 2014
					</span>
                    </li>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-camera bg-purple"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                            <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                            <div class="timeline-body">
                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                            </div>
                        </div>
                    </li>
                    <!-- END timeline item -->
                    <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                </ul>
                <!--<div class="text-center p30"><i class="fa fa-list-alt" style="font-size: 100px;"></i> <br> No posts to show</div>-->
            </div>

        </div>
    </div>
    </div>
    </div>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            function split( val ) {
                return val.split( /,\s*/ );
            }
            function extractLast( term ) {
                return split( term ).pop();
            }

            $( "#sendto" )
            // don't navigate away from the field on tab when selecting an item
                    .on( "keydown", function( event ) {
                        if ( event.keyCode === $.ui.keyCode.TAB &&
                            $( this ).autocomplete( "instance" ).menu.active ) {
                            event.preventDefault();
                        }
                    })
                    .autocomplete({
                        source: function( request, response ) {
                            $.getJSON( "<?php echo url(config('laraadmin.adminRoute') . '/autosuggestuser'); ?>", {
                                term: extractLast( request.term )
                            }, response );
                        },
                        search: function() {
                            var term = extractLast( this.value );
                            if ( term.length < 0 ) {
                                return false;
                            }
                        },
                        focus: function() {
                            return false;
                        },
                        select: function( event, ui ) {
                            var terms = split( this.value );
                            terms.pop();
                            terms.push( ui.item.value );
                            terms.push( "" );
                            this.value = terms.join( ", " );
                            return false;
                        }
                    });
        } );
    </script>
@endsection
<style>
    .alert.alert-success {
        width: 50%;
        margin: 10px auto;
        text-align: center;
        font-size: 18px;
        padding: 8px;
    }
</style>