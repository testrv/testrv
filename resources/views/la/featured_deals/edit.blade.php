@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/featured_deals') }}">Featured Deal</a> :
@endsection
@section("contentheader_description", $featured_deal->$view_col)
@section("section", "Featured Deals")
@section("section_url", url(config('laraadmin.adminRoute') . '/featured_deals'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Featured Deals Edit : ".$featured_deal->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($featured_deal, ['route' => [config('laraadmin.adminRoute') . '.featured_deals.update', $featured_deal->id ], 'method'=>'PUT', 'id' => 'featured_deal-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'deal_title')
					@la_input($module, 'deal_name')
					@la_input($module, 'deal_address')
					@la_input($module, 'deal_phone')
					@la_input($module, 'deal_price')
					@la_input($module, 'deal_image')
					@la_input($module, 'deal_description')
					@la_input($module, 'deal_order')
					@la_input($module, 'deal_archived')
					@la_input($module, 'deal_type')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/featured_deals') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#featured_deal-edit-form").validate({
		
	});
});
</script>
@endpush
