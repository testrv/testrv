@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/explore_deals') }}">Explore Deal</a> :
@endsection
@section("contentheader_description", $explore_deals->$view_col)
@section("section", "Explore Deals")
@section("section_url", url(config('laraadmin.adminRoute') . '/explore_deals'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Explore Deals Edit : ".$explore_deals->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($explore_deals, ['route' => [config('laraadmin.adminRoute') . '.explore_deals.update', $explore_deals->id ], 'method'=>'PUT', 'id' => 'explore_deal-edit-form']) !!}
					@la_form($module)

				{{--@la_display($module, 'deal_title')--}}
				{{--@la_display($module, 'deal_name')--}}
				{{--@la_display($module, 'deal_address')--}}
				{{--@la_display($module, 'deal_phone')--}}
				{{--@la_display($module, 'deal_price')--}}
				{{--@la_display($module, 'deal_image')--}}
				{{--@la_display($module, 'deal_description')--}}
				{{--@la_display($module, 'deal_order')--}}
				{{--@la_display($module, 'deal_archived')--}}
				{{--@la_display($module, 'deal_type')--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/explore_deals') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#explore_deal-edit-form").validate({
		
	});
});
</script>
@endpush
